import time
import pandas as pd
import numpy as np
import mlrose
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

# read, transform, and split dataset
# https://archive.ics.uci.edu/ml/datasets/EEG+Eye+State
data = pd.read_csv("datasets/eeg_eye_state.csv")
scaler = StandardScaler()
X = data.drop(["class"], axis=1).copy()
y = data["class"].copy()
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0, shuffle=True)
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

iter_schedule = np.linspace(1, 200, 10)
for i in range(len(iter_schedule)):
    iter_schedule[i] = int(iter_schedule[i])

rhc_train_acc = []
sa_train_acc = []
ga_train_acc = []
gd_train_acc = []
rhc_train_time = []
sa_train_time = []
ga_train_time = []
gd_train_time = []
rhc_test_acc = []
sa_test_acc = []
ga_test_acc = []
gd_test_acc = []
rhc_test_time = []
sa_test_time = []
ga_test_time = []
gd_test_time = []

for i in iter_schedule:
    print(i, "iterations...")

    np.random.seed(3)
    print("RHC...")
    nn_model1 = mlrose.NeuralNetwork(hidden_nodes = [10, 10, 10], activation ='tanh', 
                                    algorithm ='random_hill_climb', 
                                    max_iters = i, bias = True, is_classifier = True, 
                                    learning_rate = 0.0001, early_stopping = True, 
                                    clip_max = 5, max_attempts = 100)
    t0 = time.time()
    nn_model1.fit(X_train, y_train)
    rhc_train_time.append(time.time() - t0)
    y_train_pred = nn_model1.predict(X_train)
    y_train_accuracy = accuracy_score(y_train, y_train_pred)
    rhc_train_acc.append(y_train_accuracy)
    t0 = time.time()
    y_test_pred = nn_model1.predict(X_test)
    rhc_test_time.append(time.time() - t0)
    y_test_accuracy = accuracy_score(y_test, y_test_pred)
    rhc_test_acc.append(y_test_accuracy)

    np.random.seed(3)
    print("SA...")
    nn_model2 = mlrose.NeuralNetwork(hidden_nodes = [10, 10, 10], activation ='tanh', 
                                    algorithm ='simulated_annealing', 
                                    max_iters = i, bias = True, is_classifier = True, 
                                    learning_rate = 0.0001, early_stopping = True, 
                                    clip_max = 5, max_attempts = 100)
    t0 = time.time()
    nn_model2.fit(X_train, y_train)
    sa_train_time.append(time.time() - t0)
    y_train_pred = nn_model2.predict(X_train)
    y_train_accuracy = accuracy_score(y_train, y_train_pred)
    sa_train_acc.append(y_train_accuracy)
    t0 = time.time()
    y_test_pred = nn_model2.predict(X_test)
    sa_test_time.append(time.time() - t0)
    y_test_accuracy = accuracy_score(y_test, y_test_pred)
    sa_test_acc.append(y_test_accuracy)

    np.random.seed(3)
    print("GA...")
    nn_model3 = mlrose.NeuralNetwork(hidden_nodes = [10, 10, 10], activation ='tanh', 
                                    algorithm ='genetic_alg', 
                                    max_iters = i, bias = True, is_classifier = True, 
                                    learning_rate = 0.0001, early_stopping = True, 
                                    clip_max = 5, max_attempts = 100)
    t0 = time.time()
    nn_model3.fit(X_train, y_train)
    ga_train_time.append(time.time() - t0)
    y_train_pred = nn_model3.predict(X_train)
    y_train_accuracy = accuracy_score(y_train, y_train_pred)
    ga_train_acc.append(y_train_accuracy)
    t0 = time.time()
    y_test_pred = nn_model3.predict(X_test)
    ga_test_time.append(time.time() - t0)
    y_test_accuracy = accuracy_score(y_test, y_test_pred)
    ga_test_acc.append(y_test_accuracy)

    np.random.seed(3)
    print("GD...")
    nn_model4 = mlrose.NeuralNetwork(hidden_nodes = [10, 10, 10], activation ='tanh', 
                                    algorithm ='gradient_descent', 
                                    max_iters = i, bias = True, is_classifier = True, 
                                    learning_rate = 0.0001, early_stopping = True, 
                                    clip_max = 5, max_attempts = 100)
    t0 = time.time()
    nn_model4.fit(X_train, y_train)
    gd_train_time.append(time.time() - t0)
    y_train_pred = nn_model4.predict(X_train)
    y_train_accuracy = accuracy_score(y_train, y_train_pred)
    gd_train_acc.append(y_train_accuracy)
    t0 = time.time()
    y_test_pred = nn_model4.predict(X_test)
    gd_test_time.append(time.time() - t0)
    y_test_accuracy = accuracy_score(y_test, y_test_pred)
    gd_test_acc.append(y_test_accuracy)


plt.subplot(2, 1, 1)
plt.plot(iter_schedule, ga_train_acc, label="GA")
plt.plot(iter_schedule, rhc_train_acc, label="RHC")
plt.plot(iter_schedule, sa_train_acc, label="SA")
plt.plot(iter_schedule, gd_train_acc, label="GD")
plt.title("Neural Network")
plt.ylabel("Training Accuracy")

plt.subplot(2, 1, 2)
plt.plot(iter_schedule, ga_train_time, label="GA")
plt.plot(iter_schedule, rhc_train_time, label="RHC")
plt.plot(iter_schedule, sa_train_time, label="SA")
plt.plot(iter_schedule, gd_train_time, label="GD")
plt.yscale("log")
plt.ylabel("Training Time (s)")

plt.xlabel("Iterations")
plt.legend(loc="best")
plt.savefig("neuralnetwork_train.png")

plt.clf()

plt.subplot(2, 1, 1)
plt.plot(iter_schedule, ga_test_acc, label="GA")
plt.plot(iter_schedule, rhc_test_acc, label="RHC")
plt.plot(iter_schedule, sa_test_acc, label="SA")
plt.plot(iter_schedule, gd_test_acc, label="GD")
plt.title("Neural Network")
plt.ylabel("Testing Accuracy")

plt.subplot(2, 1, 2)
plt.plot(iter_schedule, ga_test_time, label="GA")
plt.plot(iter_schedule, rhc_test_time, label="RHC")
plt.plot(iter_schedule, sa_test_time, label="SA")
plt.plot(iter_schedule, gd_test_time, label="GD")
plt.ylabel("Testing Time (s)")

plt.xlabel("Iterations")
plt.legend(loc="best")
plt.savefig("neuralnetwork_test.png")