Link to code repo: https://bitbucket.org/abemelvin/ml2/src/master/

In addition to the standard scientific packages, this code repository relies on the mlrose python package.

To reproduce the neural network graphs, execute 'python neural_network.py'.
The outputs are 'neuralnetwork_train.png' and 'neuralnetwork_test.png'.

To reproduce the knapsack graphs, execute 'python knapsack.py'. The output is 'knapsack.png'.

To reproduce the six peaks graphs, execute 'python sixpeaks.py'. The output is 'sixpeaks.png'.

To reproduce the flip flop graphs, execute 'python flipflop.py'. The output is 'flipflop.png'.
In addition, 'python rhc.py' and 'python sa.py' will reproduce the graphs 'rhc.png' and 'sa.png'.
These graphs were the results of exploring the parameter space of RHC and SA respectively.