import mlrose
import numpy as np
from matplotlib import pyplot as plt
import time

np.random.seed(3)

# Initialize fitness function object
fitness = mlrose.FlipFlop()

# Define optimization problem object
problem_fit = mlrose.DiscreteOpt(length=50, fitness_fn=fitness, maximize=True, max_val=2)

iter_schedule = np.linspace(0, 2500, 25)
for i in range(len(iter_schedule)):
    iter_schedule[i] = int(iter_schedule[i])

# simulated annealing
print("Simulated annealing exp...")
schedule = mlrose.ArithDecay()
sa_fitness_arith = []
sa_time_arith = []
for i in iter_schedule:
    np.random.seed(3)
    t0 = time.time()
    best_state, best_fitness = mlrose.simulated_annealing(problem_fit, schedule, max_iters=i)
    sa_time_arith.append(time.time() - t0)
    sa_fitness_arith.append(best_fitness)

# simulated annealing
print("Simulated annealing geom...")
schedule = mlrose.GeomDecay()
sa_fitness_geom = []
sa_time_geom = []
for i in iter_schedule:
    np.random.seed(3)
    t0 = time.time()
    best_state, best_fitness = mlrose.simulated_annealing(problem_fit, schedule, max_iters=i)
    sa_time_geom.append(time.time() - t0)
    sa_fitness_geom.append(best_fitness)

# simulated annealing
print("Simulated annealing exp...")
schedule = mlrose.ExpDecay()
sa_fitness_exp = []
sa_time_exp = []
for i in iter_schedule:
    np.random.seed(3)
    t0 = time.time()
    best_state, best_fitness = mlrose.simulated_annealing(problem_fit, schedule, max_iters=i)
    sa_time_exp.append(time.time() - t0)
    sa_fitness_exp.append(best_fitness)


plt.subplot(2, 1, 1)
plt.plot(iter_schedule, sa_fitness_arith, label="Arithmetic")
plt.plot(iter_schedule, sa_fitness_geom, label="Geometric")
plt.plot(iter_schedule, sa_fitness_exp, label="Exponential")
plt.title("Flip Flop")
plt.ylabel("Fitness")

plt.subplot(2, 1, 2)
plt.plot(iter_schedule, sa_time_arith, label="Arithmetic")
plt.plot(iter_schedule, sa_time_geom, label="Geometric")
plt.plot(iter_schedule, sa_time_exp, label="Exponential")
plt.legend(loc="best")
plt.yscale("log")
plt.xlabel("Iterations")
plt.ylabel("Time (s)")
plt.savefig("sa.png")