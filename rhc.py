import mlrose
import numpy as np
from matplotlib import pyplot as plt
import time

np.random.seed(3)

# Initialize fitness function object
fitness = mlrose.FlipFlop()

# Define optimization problem object
problem_fit = mlrose.DiscreteOpt(length=50, fitness_fn=fitness, maximize=True, max_val=2)

iter_schedule = np.linspace(0, 500, 25)
for i in range(len(iter_schedule)):
    iter_schedule[i] = int(iter_schedule[i])


# randomized hill climbing
print("Randomized hill climbing 0...")
rhc_fitness_zero = []
rhc_time_zero = []
for i in iter_schedule:
    np.random.seed(3)
    t0 = time.time()
    best_state, best_fitness = mlrose.random_hill_climb(problem_fit, max_iters=i, restarts=0)
    rhc_time_zero.append(time.time() - t0)
    rhc_fitness_zero.append(best_fitness)

# randomized hill climbing
print("Randomized hill climbing 10...")
rhc_fitness_ten = []
rhc_time_ten = []
for i in iter_schedule:
    np.random.seed(3)
    t0 = time.time()
    best_state, best_fitness = mlrose.random_hill_climb(problem_fit, max_iters=i, restarts=10)
    rhc_time_ten.append(time.time() - t0)
    rhc_fitness_ten.append(best_fitness)

# randomized hill climbing
print("Randomized hill climbing 100...")
rhc_fitness_hundred = []
rhc_time_hundred = []
for i in iter_schedule:
    np.random.seed(3)
    t0 = time.time()
    best_state, best_fitness = mlrose.random_hill_climb(problem_fit, max_iters=i, restarts=100)
    rhc_time_hundred.append(time.time() - t0)
    rhc_fitness_hundred.append(best_fitness)

# randomized hill climbing
print("Randomized hill climbing 1000...")
rhc_fitness_thousand = []
rhc_time_thousand = []
for i in iter_schedule:
    np.random.seed(3)
    t0 = time.time()
    best_state, best_fitness = mlrose.random_hill_climb(problem_fit, max_iters=i, restarts=1000)
    rhc_time_thousand.append(time.time() - t0)
    rhc_fitness_thousand.append(best_fitness)

# randomized hill climbing
print("Randomized hill climbing 10000...")
rhc_fitness_tenthousand = []
rhc_time_tenthousand = []
for i in iter_schedule:
    np.random.seed(3)
    t0 = time.time()
    best_state, best_fitness = mlrose.random_hill_climb(problem_fit, max_iters=i, restarts=10000)
    rhc_time_tenthousand.append(time.time() - t0)
    rhc_fitness_tenthousand.append(best_fitness)


plt.subplot(2, 1, 1)
plt.plot(iter_schedule, rhc_fitness_zero, label="0")
plt.plot(iter_schedule, rhc_fitness_ten, label="10")
plt.plot(iter_schedule, rhc_fitness_hundred, label="100")
plt.plot(iter_schedule, rhc_fitness_thousand, label="1000")
plt.plot(iter_schedule, rhc_fitness_tenthousand, label="10000")
plt.title("Flip Flop")
plt.ylabel("Fitness")

plt.subplot(2, 1, 2)
plt.plot(iter_schedule, rhc_time_zero, label="0")
plt.plot(iter_schedule, rhc_time_ten, label="10")
plt.plot(iter_schedule, rhc_time_hundred, label="100")
plt.plot(iter_schedule, rhc_time_thousand, label="1000")
plt.plot(iter_schedule, rhc_time_tenthousand, label="10000")
plt.legend(loc="best")
plt.yscale("log")
plt.xlabel("Iterations")
plt.ylabel("Time (s)")
plt.savefig("rhc.png")