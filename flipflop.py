import mlrose
import numpy as np
from matplotlib import pyplot as plt
import time

np.random.seed(3)

# Initialize fitness function object
fitness = mlrose.FlipFlop()

# Define optimization problem object
problem_fit = mlrose.DiscreteOpt(length=50, fitness_fn=fitness, maximize=True, max_val=2)

iter_schedule = np.linspace(0, 2500, 25)
for i in range(len(iter_schedule)):
    iter_schedule[i] = int(iter_schedule[i])

# genetic algorithm
print("Genetic algorithm...")
ga_fitness = []
ga_time = []
for i in iter_schedule:
    np.random.seed(3)
    t0 = time.time()
    best_state, best_fitness = mlrose.genetic_alg(problem_fit, mutation_prob=0.1, max_iters=i)
    ga_time.append(time.time() - t0)
    ga_fitness.append(best_fitness)

# randomized hill climbing
print("Randomized hill climbing...")
rhc_fitness = []
rhc_time = []
for i in iter_schedule:
    np.random.seed(3)
    t0 = time.time()
    best_state, best_fitness = mlrose.random_hill_climb(problem_fit, max_iters=i, restarts=0)
    rhc_time.append(time.time() - t0)
    rhc_fitness.append(best_fitness)

# simulated annealing
print("Simulated annealing...")
schedule = mlrose.ArithDecay()
sa_fitness = []
sa_time = []
for i in iter_schedule:
    np.random.seed(3)
    t0 = time.time()
    best_state, best_fitness = mlrose.simulated_annealing(problem_fit, schedule=schedule, max_iters=i)
    sa_time.append(time.time() - t0)
    sa_fitness.append(best_fitness)

# mimic
print("MIMIC...")
mimic_fitness = []
mimic_time = []
for i in iter_schedule:
    np.random.seed(3)
    t0 = time.time()
    best_state, best_fitness = mlrose.mimic(problem_fit, max_iters=i)
    mimic_time.append(time.time() - t0)
    mimic_fitness.append(best_fitness)

plt.subplot(2, 1, 1)
plt.plot(iter_schedule, ga_fitness, label="GA")
plt.plot(iter_schedule, rhc_fitness, label="RHC")
plt.plot(iter_schedule, sa_fitness, label="SA")
plt.plot(iter_schedule, mimic_fitness, label="MIMIC")
plt.title("Flip Flop")
plt.ylabel("Fitness")

plt.subplot(2, 1, 2)
plt.plot(iter_schedule, ga_time, label="GA")
plt.plot(iter_schedule, rhc_time, label="RHC")
plt.plot(iter_schedule, sa_time, label="SA")
plt.plot(iter_schedule, mimic_time, label="MIMIC")
plt.legend(loc="best")
plt.yscale("log")
plt.xlabel("Iterations")
plt.ylabel("Time (s)")
plt.savefig("flipflop.png")